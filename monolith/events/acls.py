import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY



def get_photo(city, state):
    # url = f"https://api.pexels.com/v1/search?query={city}&{state}/"
    base_url_photo = "https://api.pexels.com/v1/search"
    params_photo = {
        "per_page": 2,
        "query": f"{city} {state}",
    }

    headers = {"Authorization": PEXELS_API_KEY}
    response_photo = requests.get(base_url_photo, headers=headers, params=params_photo)
    content_photo = response_photo.json()

    try:
        return {"picture_url": content_photo["photos"][1]["src"]["original"]}

    except (KeyError, IndexError):
        if KeyError:
            return {"The error is ": "KeyError in get_photo"}
        else:
            return {"The error is ": "Indexerror in get_photo"}
    

def get_weather_data(city, state):
    base_url_coord = "http://api.openweathermap.org/geo/1.0/direct"
    params_coord = {
        "q": f"{city}, {state}, US",
        "appid": OPEN_WEATHER_API_KEY,        
    }

    response_coord = requests.get(base_url_coord, params=params_coord)
    if response_coord.status_code != 200:
        return  {"The error is": "Failed to get coordinates"}
        
    content_coord = response_coord.json()

    base_url_weather = "https://api.openweathermap.org/data/2.5/weather"
    params_weather = {
        "lat": content_coord[0]["lat"],
        "lon": content_coord[0]["lon"],
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }

    response_weather = requests.get(base_url_weather, params=params_weather)
    if response_weather.status_code != 200:
        return {"The error is": "Failed to get weather"}
    
    content_weather = response_weather.json()

    try:
        return {
            "weather": content_weather["weather"][0]["description"],
            "temp": content_weather["main"]["temp"]
        }

    except (KeyError, IndexError):
        if KeyError:
            return {"The error is ": "KeyError in get_weather"}
        else:
            return {"The error is ": "Indexerror in get_weather"}






    
